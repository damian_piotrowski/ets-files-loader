#include "debug.h"
#include <QtWidgets/qapplication.h>
#include <application/mainwindow.h>

int main(int argc, char *argv[]) {
  Logger::getInstance().setLogFile("logs.log", true);
  DBG_1("Compiled: ", __DATE__, " ", __TIME__);

  QApplication a(argc, argv);
  MainWindow w;
  w.show();

  int result = a.exec();
  DBG_1("Exit: ", result);
  return result;
}
