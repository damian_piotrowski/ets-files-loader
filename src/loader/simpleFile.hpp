#ifndef simplefile_hpp
#define simplefile_hpp

#include <fstream>
#include <string>

class SimpleFile {
public:
  enum direction { unknown = 0, readFromFile = 1, writeToFile = 2 };

  /**
   * Default constructor
   */
  SimpleFile();

  /**
   * Destructor
   */
  ~SimpleFile();

  /**
   * Opens file
   * @param filePath  path to file to be opened
   * @param direction direction of stream read/write
   */
  void open(const std::string filePath = "",
            direction streamDirection = unknown);

  /**
   * Close previously opened file
   */
  void close();

  /**
   * @return position of the current character in opened stream
   */
  std::streampos getPosition();

  /**
   * @param position new position to be set
   * @param way mode to move cursor, move by current position or from begining
   * of stream
   */
  void setPosition(std::streampos position,
                   std::ios_base::seekdir way = std::ios_base::beg);

  /**
   * Reads data from previously opened stream
   * @param data pointer to variable where data will be stored
   * @param size amount of data to be read
   * @param offset if offset is other than 0, then before read cursor is move by
   * this value
   * @param way mode to move cursor if offset is other than 0, it can be moved
   * from begining of stream or by old position
   */
  void read(char *data, int size, std::streampos offset = 0,
            std::ios_base::seekdir way = std::ios_base::cur);

  /**
   * Move forward by given amount of bytes
   * @param bytes amount of bytes to be skipped
   */
  void skipBytes(std::streampos bytes);

private:
  std::string fileName = "";
  std::ifstream file;
  direction streamDirection = unknown;
};

#endif // simplefile_hpp
