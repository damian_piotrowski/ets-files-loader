#include "debug.h"
#include <loader/simpleFile.hpp>
#include <shared/logger.hpp>
#include <shared/structures.h>

void SimpleFile::open(const std::string filePath, direction streamDirection) {
  DBG_1("OpenFile: ", filePath);
  DBG_2("streamDirection: ", streamDirection);
  if (streamDirection == unknown) {
    if (this->streamDirection == unknown) {
      throw std::string("Unknown stream direction");
    }
  } else {
    this->streamDirection = streamDirection;
  }
  if (filePath.length()) {
    this->fileName = filePath;
  }

  DBG_1("Opening file \"", this->fileName, "\"");
  if (!fileName.length()) {
    throw std::string("Unknown fileName");
  }
  file.open(fileName.c_str(), std::ios::in | std::ios::binary);
  if (!file.is_open()) {
    throw std::string("Failed to open");
  }
}

void SimpleFile::close() {
  if (file.is_open()) {
    DBG_1("Closing file \"", fileName, "\"");
    file.close();
  }
}

SimpleFile::SimpleFile() {}

SimpleFile::~SimpleFile() { close(); }

void SimpleFile::skipBytes(std::streampos bytes) {
  setPosition(bytes, std::ios_base::cur);
}

std::streampos SimpleFile::getPosition() {
  if (!file.is_open()) {
    throw std::string("Cannot read from file, its not open");
  }
  return file.tellg();
}

void SimpleFile::setPosition(std::streampos position,
                             std::ios_base::seekdir way) {
  DBG_2("SetPosition: ", position);
  if (!file.is_open()) {
    throw std::string("Cannot read from file, its not open");
  }
  file.seekg(position, way);
}

void SimpleFile::read(char *data, int size, std::streampos offset,
                      std::ios_base::seekdir way) {
  if (!file.is_open()) {
    throw std::string("Cannot read from file, its not open");
  }
  if (offset) {
    setPosition(offset, way);
  }
  DBG_2("read size:", size, " offset:", offset, " position:", getPosition());
  file.read(data, size);
}
