#ifndef debug_h
#define debug_h

#include <shared/debugs.h>

#define DBG_PREFIX "<Simple File>"
#define DBG_LEVEL SIMPLE_FILE_DBG_LEVEL

#endif // debug_h
