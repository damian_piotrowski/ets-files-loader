#ifndef debug_h
#define debug_h

#include <shared/debugs.h>

#define DBG_PREFIX "<Ets Loader>"
#define DBG_LEVEL ETS_FILE_LOADER_DBG_LEVEL

#endif // debug_h
