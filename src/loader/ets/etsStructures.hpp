#ifndef etsStructures_hpp
#define etsStructures_hpp

#include <string>
#include <stdint.h>

struct header_t {
  char magic[4];
  uint32_t size;
  uint32_t version;
  uint32_t dimension;
  int64_t head2offset;
  uint32_t head2size;
  uint32_t unknown1;
  int64_t chunkOffset;
  uint32_t chunks;
  uint32_t unknown2;
};

struct header2_t {
  char magic[4];
  uint32_t version; // 0x30001 0x30003
  uint32_t pixelType;
  uint32_t sizeC;
  uint32_t colorSpace;
  uint32_t compressionType = 0; // 2 jpeg  3 jpeg2000
  uint32_t compressionQuality = 0;
  uint32_t tileX = 0; // 512
  uint32_t tileY = 0; // 512
  uint32_t tileZ = 0; // 1
};

// todo base chunk less informations
struct chunk_t {
  uint32_t unknown1;
  uint32_t coord[3];
  uint32_t level;
  uint64_t offset;
  uint32_t size;
  uint32_t unknown2;
};

struct image_t {
  uint32_t size;
  char *data_ptr;

  image_t() : size(0), data_ptr(NULL) {}
  ~image_t() {
    if (data_ptr != NULL) {
      delete[] data_ptr;
    }
  }
};

struct level_size_t {
  uint32_t horizontal;
  uint32_t vertical;
  uint32_t depth;

  level_size_t() : horizontal(0), vertical(0), depth(0) {}

  level_size_t(const level_size_t &level)
      : horizontal(level.horizontal), vertical(level.vertical),
        depth(level.depth) {}

  void set(const level_size_t &level) {
    horizontal = level.horizontal;
    vertical = level.vertical;
    depth = level.depth;
  }

  void clear() {
    horizontal = 0;
    vertical = 0;
    depth = 0;
  }
};

std::string toString(const level_size_t &image_size);
std::string toString(const header_t &data);
std::string toString(const header2_t &data);
std::string toString(const char *data, const uint32_t size,
                     bool numeric = false);
std::string toString(const uint64_t *data, const int32_t size);
std::string toString(const chunk_t &chunk);
std::string toString(const uint8_t *bytes, uint32_t size);

#endif // etsStructures_hpp
