#include "debug.h"
#include <loader/ets/ets.hpp>
#include <shared/deletePointers.hpp>

EtsFile::EtsFile() {}

bool EtsFile::open(std::string filePath) {
  close();
  SimpleFile::open(filePath, SimpleFile::readFromFile);
  DBG_1("READING:", filePath);
  bool result = readData();

  DBG_1("READING finished");
  if (!result) {
    close();
  }

  return result;
}

void EtsFile::close() {
	m_image_details.clear();
	DBG_1("closing file");
	SimpleFile::close();
}

bool EtsFile::isOpen() {
	return !m_image_details.empty();
}

bool EtsFile::readData() {
  m_image_details.clear();
  if (readHeaders()) {
    readChunks();
    calculateSizes();
	return true;
  }
  else {
    return false;
  }
}

bool EtsFile::readHeaders() {
  bool result = true;

  read((char *)&m_head, sizeof(m_head));
  DBG_2("### HEAD ###\n", toString(m_head));

  if (strcmp(m_head.magic, "SIS") != 0) {
    DBG_1("ETS head unknown magic");
    result = false;
  } else {
    setPosition(m_head.head2offset);
    read((char *)&m_head2, sizeof(m_head2));
    DBG_2("### HEAD_2 ###\n", toString(m_head2));

    if (strcmp(m_head2.magic, "ETS") != 0) {
      DBG_1("ETS head_2 unknown magic");
      result = false;
    }
  }

  return result;
}

void EtsFile::readImage(image_t &image, const uint64_t offset) {
  setPosition(offset);
  DBG_4("reading image at: ", offset, " size:", image.size);
  image.data_ptr = new char[image.size];
  read(image.data_ptr, image.size);
}

image_t EtsFile::readImage(uint32_t level, uint32_t x, uint32_t y, uint32_t z) {
  image_t result;
  image_details_t::iterator it = m_image_details.find(level);
  DBG_2(__PRETTY_FUNCTION__, "lvl:", level, " x:", x, " y:", y, " z:", z);
  if (it != m_image_details.end()) {
    for (chunk_t chunk : it->second.second) {
      if (chunk.coord[0] == x && chunk.coord[1] == y && chunk.coord[2] == z) {
        result.size = chunk.size;
        readImage(result, chunk.offset);
        DBG_2("found img");
        break;
      }
    }
  } else {
    DBG_1("ERROR, image level not found lvl:", level);
  }

  return result;
}

uint32_t EtsFile::getTileWidth() { return m_head2.tileX; }

uint32_t EtsFile::getTileHeight() { return m_head2.tileY; }

level_size_t EtsFile::getLevelSize(const int32_t level) {
  level_size_t result;

  if (level >= 0) {
    image_details_t::iterator it = m_image_details.find(level);
    if (it != m_image_details.end()) {
      result = it->second.first;
    }
  } else {
    image_details_t::reverse_iterator it = m_image_details.rbegin();
    if (it != m_image_details.rend()) {
      result = it->second.first;
    }
  }

  return result;
}

uint32_t EtsFile::findOptimalLevel(const uint32_t width, const uint32_t height,
                                   bool bigger) {
  image_details_t::reverse_iterator it = m_image_details.rbegin();
  uint32_t level = it->first;
  uint32_t itw, ith;
  for (; it != m_image_details.rend(); ++it) {
    itw = it->second.first.vertical * getTileWidth();
    ith = it->second.first.horizontal * getTileHeight();
    DBG_2("looking for optimal, l:", it->first, " w:", itw, " h:", ith);

    if (itw > width && ith > height) {
		break;
	}

	level = it->first;
  }

  DBG_1("Optimal level for size: ", width, "x", height, " found:", level,
        " bigger_better:", bigger);
  return level;
}

void EtsFile::readChunks() {
  DBG_2("### Reading chunks ###");
  setPosition(m_head.chunkOffset);
  DBG_3("Offset: ", m_head.chunkOffset);

  for (uint32_t i = 0; i < m_head.chunks; i++) {
    chunk_t chunk;
    read((char *)&chunk.unknown1, sizeof(chunk.unknown1));
    read((char *)&chunk.coord[0], sizeof(chunk.coord[0]));
    read((char *)&chunk.coord[1], sizeof(chunk.coord[1]));
    read((char *)&chunk.coord[2], sizeof(chunk.coord[2]));
    read((char *)&chunk.level, sizeof(chunk.level));
    read((char *)&chunk.offset, sizeof(chunk.offset));
    read((char *)&chunk.size, sizeof(chunk.size));
    read((char *)&chunk.unknown2, sizeof(chunk.unknown2));

    DBG_3("CHUNK #", i);
    DBG_3(toString(chunk));

    m_image_details[chunk.level].second.push_back(chunk);
  }
  DBG_2("Reading done");
}

void EtsFile::calculateSizes() {
  DBG_1("Calculating sizes");

  int level_counter = 0;
  for (image_details_t::iterator it = m_image_details.begin();
       it != m_image_details.end(); ++it) {
    level_size_t level_size;

    for (chunks_t::iterator chunk_it = it->second.second.begin();
         chunk_it != it->second.second.end(); ++chunk_it) {
      if (chunk_it->coord[0] >= level_size.vertical) {
        level_size.vertical = chunk_it->coord[0] + 1;
      }
      if (chunk_it->coord[1] >= level_size.horizontal) {
        level_size.horizontal = chunk_it->coord[1] + 1;
      }
      if (chunk_it->coord[2] >= level_size.depth) {
        level_size.depth = chunk_it->coord[2] + 1;
      }
    }
    DBG_1("[", level_counter++, "]level: ", toString(level_size));
    it->second.first = level_size;
  }
}

