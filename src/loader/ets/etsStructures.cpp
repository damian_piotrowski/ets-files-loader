#include "etsStructures.hpp"
#include <sstream>

std::string toString(const header_t &data) {
  std::ostringstream output;

  output << "magic: " << data.magic << "\n";
  output << "size: " << data.size << "\n";
  output << "version: " << data.version << "\n";
  output << "dimension: " << data.dimension << "\n";
  output << "offset: " << data.head2offset << "\n";
  output << "size2: " << data.head2size << "\n";
  output << "unknown1: " << data.unknown1 << "\n";
  output << "chunkOffset: " << data.chunkOffset << "\n";
  output << "chunks: " << data.chunks << "\n";
  output << "unknown2: " << data.unknown2 << "\n";

  return output.str();
}

std::string toString(const header2_t &data) {
  std::ostringstream output;

  output << "magic: " << data.magic << "\n";
  output << "version: " << data.version << "\n";
  output << "pixelType: " << data.pixelType << "\n";
  output << "sizeC: " << data.sizeC << "\n";
  output << "colorSpace: " << data.colorSpace << "\n";
  output << "compressionType: " << data.compressionType << "\n";
  output << "compressionQuality: " << data.compressionQuality << "\n";
  output << "tileX: " << data.tileX << "\n";
  output << "tileY: " << data.tileY << "\n";
  output << "tileZ: " << data.tileZ << "\n";

  return output.str();
}

std::string toString(const chunk_t &chunk) {
  std::ostringstream output;

  output << "unknown1: " << chunk.unknown1 << "\n";
  output << "coord[0]: " << chunk.coord[0] << "\n";
  output << "coord[1]: " << chunk.coord[1] << "\n";
  output << "coord[2]: " << chunk.coord[2] << "\n";
  output << "level: " << chunk.level << "\n";
  output << "offset: " << chunk.offset << "\n";
  output << "size: " << chunk.size << "\n";
  output << "unknown2: " << chunk.unknown2 << "\n";

  return output.str();
}

std::string toString(const level_size_t &level_size) {
  std::ostringstream output;
  output << "v:" << level_size.vertical << " h:" << level_size.horizontal
         << " z:" << level_size.depth;

  return output.str();
}

std::string toString(const char *bytes, uint32_t size) {
  std::ostringstream output;

  for (uint32_t i = 0; i < size; i++) {
    output << bytes[i];
  }

  return output.str();
}

std::string toString(const char *data, const uint32_t size, bool numeric) {
  std::ostringstream output;

  for (uint32_t i = 0; i < size; i++) {
    output << "[" << i << "]" << (numeric ? data[i] : (int)data[i])
           << (i == size - 1 ? "" : "\n");
  }

  return output.str();
}

std::string toString(const uint32_t *data, const uint32_t size) {
  std::ostringstream output;

  for (uint32_t i = 0; i < size; i++) {
    output << "[" << i << "]" << data[i] << (i == size - 1 ? "" : "\n");
  }

  return output.str();
}
