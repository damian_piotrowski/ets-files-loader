#ifndef ets_hpp
#define ets_hpp

#include <iostream>
#include <list>
#include <loader/ets/etsStructures.hpp>
#include <loader/simpleFile.hpp>
#include <map>
#include <shared/structures.h>
#include <stdint.h>

class EtsFile : private SimpleFile {

  typedef std::list<chunk_t> chunks_t;
  typedef std::pair<level_size_t, chunks_t> level_details_t;
  typedef std::map<uint32_t, level_details_t> image_details_t;

  header_t m_head;
  header2_t m_head2;
  image_details_t m_image_details;

  bool readData();
  bool readHeaders();
  void readChunks();
  void calculateSizes();
  void readImage(image_t &image, const uint64_t offset);

public:
  // todo destructor

  /**
   * Default constructor
   */
  EtsFile();

  /**
   * @return width of single tile
   */
  uint32_t getTileWidth();

  /**
   * @return height of single tile
   */
  uint32_t getTileHeight();

  /**
   * @param width expected image minimum width
   * @param height expecet image minimum height
   * @param bigger specify if bigger image is better than smaller (default true)
   * @return level id which size is lowest size of this file but equal or bigger
   * than given sizes
   */
  uint32_t findOptimalLevel(const uint32_t width, const uint32_t height,
                            bool bigger = true);

  /**
   * @param level id of image's level, if level is equal -1, target is lowest
   * level
   * @return sizes of level identified by param level if level doesnt exsists
   * returns default level_size_t
   */
  level_size_t getLevelSize(const int32_t level);

  /**
   * @param level id of level
   * @param x x-coord
   * @param y y-coord
   * @param z z-coord
   * @return image data of chunk identified by parametrs
   */
  image_t readImage(uint32_t level, uint32_t x, uint32_t y, uint32_t z);

  /**
   * Opens file and read ETS structures.
   * @param filePath path to ETS file
   * @return true if success, false otherwise
   */
  bool open(std::string filePath = "");

  /**
   * Close opened file
   */
  void close();

  /**
   * @return true if file is open, fale otherwise
   */
  bool isOpen();
};

#endif // ets_hpp
