#ifndef debug_h
#define debug_h

#include <shared/debugs.h>

#define DBG_PREFIX "<main.cpp>"
#define DBG_LEVEL MAIN_FILE_DBG_LEVEL

#endif // debug_h
