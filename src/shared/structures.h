#ifndef structures_h
#define structures_h

#include <sstream>
#include <stdint.h>

typedef unsigned char BYTE_t;
typedef uint32_t DWORD_t;
typedef uint16_t WORD_t;

std::string to_string(const DWORD_t val);

struct Point {
  DWORD_t x;
  DWORD_t y;
  Point(const DWORD_t x = 0, const DWORD_t y = 0) {
    this->x = x;
    this->y = y;
  }
};

struct RGBpixel {
  BYTE_t red = 0;
  BYTE_t green = 0;
  BYTE_t blue = 0;
};

struct Rational {
  WORD_t numerator = 0;
  WORD_t denominator = 0;
};

#endif // structures_h
