#ifndef singleton_h
#define singleton_h

class Singleton {
private:
  Singleton();
  Singleton(const Singleton &);
  ~Singleton();

public:
  virtual Singleton &getInstance();
};

#endif // singleton_h
