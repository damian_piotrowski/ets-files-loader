#ifndef logger_hpp
#define logger_hpp

#include <fstream>
#include <iostream>
#include <shared/singleton.h>
#include <sstream>
#include <string>
#include <time.h>

#define LOG(...) Logger::getInstance().Log(__VA_ARGS__)

class Logger {
private:
  std::ofstream log_file;
  std::string log_file_name;

  Logger() {}

  ~Logger() {
    if (log_file.is_open()) {
      log_file.close();
    }
  }

  template <typename T> void log_func_simple(std::ostringstream &output, T t) {
    output << t;
  }

  template <typename T, typename... Args>
  void log_func_simple(std::ostringstream &output, T t, Args... args) {
    output << t;
    log_func_simple(output, args...);
  }

public:
  template <typename... Args> void Log(Args... args) {
    time_t now = time(0);
    tm *ltm = localtime(&now);
    std::ostringstream output;

    if (ltm->tm_hour < 10)
      output << "0";
    output << ltm->tm_hour << ":";
    if (ltm->tm_min < 10)
      output << "0";
    output << ltm->tm_min << ":";
    if (ltm->tm_sec < 10)
      output << "0";
    output << ltm->tm_sec << " ";

    log_func_simple(output, args...);
    output << "\n";

    if (log_file.is_open()) {
      log_file << output.str();
      //        log_file.flush();
    }
    std::cout << output.str();
  }

  void setLogFile(std::string fileName, bool clearLog = false) {
    if (log_file_name != fileName) {
      if (log_file.is_open()) {
        log_file.close();
      }
      log_file_name = fileName;
      std::ios_base::openmode mode = std::ofstream::out;
      if (!clearLog) {
        mode = mode | std::ofstream::app;
      }
      log_file.open(log_file_name.c_str(), mode);
      if (!log_file.is_open()) {
        Log("Failed to open logFile");
      } else {
        Log("Logs initialized");
      }
    }
  }

  static Logger &getInstance() {
    static Logger logFile;
    return logFile;
  }
};

#endif // logger_hpp
