#ifndef debugs_h
#define debugs_h

#include <shared/logger.hpp>

//#define NO_DEBUGS

#define MAIN_FILE_DBG_LEVEL 1
#define ETS_FILE_LOADER_DBG_LEVEL 0
#define TIFF_FILE_LOADER_DBG_LEVEL 0
#define SIMPLE_FILE_DBG_LEVEL 0
#define IMAGE_AREA_DBG_LEVEL 1
#define MAIN_WINDOW_DBG_LEVEL 0

// ---------------------------------------------------

#ifdef NO_DEBUGS
#define _DBG_(...)
#else
#define _DBG_(_CALLED_LVL_, ...)                                               \
  ((DBG_LEVEL) >= (_CALLED_LVL_) ? (LOG(DBG_PREFIX, __VA_ARGS__)) : ((void)0))
#endif

#define DBG_1(...) _DBG_(1, __VA_ARGS__)
#define DBG_2(...) _DBG_(2, __VA_ARGS__)
#define DBG_3(...) _DBG_(3, __VA_ARGS__)
#define DBG_4(...) _DBG_(4, __VA_ARGS__)

#endif // debugs_h
