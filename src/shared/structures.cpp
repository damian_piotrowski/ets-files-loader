#include "structures.h"

std::string to_string(const DWORD_t val) {
  std::ostringstream oss;
  oss << val;
  return oss.str();
}
