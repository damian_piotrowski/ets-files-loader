#ifndef getSecs_hpp
#define getSecs_hpp

class GetSecs {
private:
  LARGE_INTEGER uFreq;
  bool bUseQPC;

public:
  double get() // zwraca milisekundy
  {
    if (!bUseQPC)
      return GetTickCount() / 1000.0f;
    else {
      LARGE_INTEGER uTicks;
      QueryPerformanceCounter(&uTicks);
      return ((double)(uTicks.QuadPart / (double)uFreq.QuadPart));
    }
  }
  GetSecs() { bUseQPC = (QueryPerformanceFrequency(&uFreq) != 0); }
};

#endif // getSecs_hpp
