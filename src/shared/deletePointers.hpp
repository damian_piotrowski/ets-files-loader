#ifndef deletepointers_hpp
#define deletepointers_hpp

template <class T> void SafeDelete(T *&pointer) {
  delete pointer;
  pointer = nullptr;
}

template <class T> void SafeDeleteArray(T *&pointer) {
  delete[] pointer;
  pointer = nullptr;
}

#endif // deletepointers_hpp
