#ifndef mainwindow_h
#define mainwindow_h

#include <QtWidgets/QFileDialog>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QLineEdit>
#include <application/imageArea/imageArea.h>
#include <stdint.h>

class MainWindow : public QMainWindow {
  Q_OBJECT
  ImageArea *m_draw_area;

  QPushButton *m_button_open_ptr;

  const uint32_t m_minimal_width = 400;
  const uint32_t m_minimal_height = 400;
  const uint32_t m_base_width = 800;
  const uint32_t m_base_height = 600;

public:
  MainWindow(QWidget *parent = 0);
  ~MainWindow();
  void OpenFile(const QString &file_path);

public slots:
  void ClickedOpen();

private:
  void resizeEvent(QResizeEvent * event);
  void print();
};

#endif // mainwindow_h
