#ifndef chunks_h
#define chunks_h

#include <QtWidgets/QLabel>
#include <list>
#include <loader/ets/ets.hpp>
#include <stdint.h>

class Chunks : public QLabel {
public:
  enum scale_e { OUT, IN, DEFAULT };

  struct point_t {
    int32_t x;
    int32_t y;
  };

private:
  EtsFile m_ets_file;
  QPoint m_position;
  point_t m_view_size = {0, 0};
  uint32_t m_level = 0;
  point_t m_tile_size_scalled = {0, 0};
  float m_scale = 0.5f;
  point_t m_loaded_tiles[2];
  point_t m_skipped_tiles = {0, 0};
  float m_xRatio = 0.0f;

  void clear();
  void loadTiles(const point_t &minimum, const point_t &maximum,
                 const uint32_t level);
  bool findOptimalLevel();
  void fixPosition();
  void loadFragment(const point_t &minimum, const point_t &maximum,
                    bool force_reload);
  void showImage(const bool force_reload = false);

public:
  Chunks(QWidget *parent);
  void openEts(const QString &file_name);
  void setViewSize(const uint32_t width, const uint32_t height);
  void zoom(const scale_e mode);
  void moveImage(const QPoint &point);
  ~Chunks();
};

#endif // chunks_h
