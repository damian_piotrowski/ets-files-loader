#include <QtGui/QImageReader>
#include <QtGui/QMouseEvent>
#include <QtGui/QPainter>
#include <application/imageArea/debug.h>
#include <application/imageArea/imageArea.h>

ImageArea::ImageArea(QWidget *parent)
    : QLabel(parent), m_parent_ptr(parent), m_chunks(this),
      m_start_position(0, 0) {
  setScaledContents(true);
  setStyleSheet("background-color: #324543;");
}

ImageArea::~ImageArea() {}

void ImageArea::Resize() {
  resize(m_parent_ptr->size().width() - 160,
         m_parent_ptr->size().height() - 10);
  move(155, 5);
  DBG_2("image area size: ", size().width(), ":", size().height());
  m_chunks.setViewSize(size().width(), size().height());
}

void ImageArea::OpenEts(const QString &fileName) {
  Resize();
  m_chunks.openEts(fileName);
}

void ImageArea::mousePressEvent(QMouseEvent *event) {
  if (event->buttons() == Qt::LeftButton) {
    m_start_position = event->pos();
  }
}

void ImageArea::mouseMoveEvent(QMouseEvent *event) {
  if (event->buttons() == Qt::LeftButton) {
    QPoint actual_position = event->pos();
    QPoint new_position(m_chunks.pos() + actual_position - m_start_position);
    QPoint center_position(m_parent_ptr->size().width() / 2,
                           m_parent_ptr->size().height() / 2);

    new_position = m_chunks.pos() + actual_position - m_start_position;
	m_chunks.moveImage(new_position);
    m_start_position = actual_position;
  }
}

void ImageArea::wheelEvent(QWheelEvent *event) {
  if (event->delta() > 0) {
    m_chunks.zoom(Chunks::IN);
  } else {
    m_chunks.zoom(Chunks::OUT);
  }
}

