#ifndef debug_h
#define debug_h

#include <shared/debugs.h>

#define DBG_PREFIX "<ImageArea>"
#define DBG_LEVEL IMAGE_AREA_DBG_LEVEL

#endif // debug_h
