#ifndef imageArea_h
#define imageArea_h

#include <QtGui/QImage>
#include <QtWidgets/QLabel>
#include <application/imageArea/chunks.h>
#include <stdint.h>

class ImageArea : public QLabel {
  Q_OBJECT
  QPixmap m_foreground;
  QWidget *m_parent_ptr;
  Chunks m_chunks;
  QPoint m_start_position;

public:
  ImageArea(QWidget *parent);
  ~ImageArea();
  void OpenEts(const QString &fileName);
  void Resize();

private:
  void mouseMoveEvent(QMouseEvent *event);
  void mousePressEvent(QMouseEvent *event);
  void wheelEvent(QWheelEvent *event);
};

#endif // imageArea_h
