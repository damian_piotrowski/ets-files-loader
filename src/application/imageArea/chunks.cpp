#include <application/imageArea/chunks.h>
#include <application/imageArea/debug.h>
#include <loader/ets/etsStructures.hpp>
#include <QPainter>

void Chunks::moveImage(const QPoint &point) {
  move(point);
  showImage();
}

void Chunks::zoom(const Chunks::scale_e mode) {
  switch(mode) {
  case OUT:
	m_scale -= 0.1f;
	break;
  case IN:
	m_scale += 0.1f;
	break;
  case DEFAULT:
    m_scale = 1.0f;	
  }

  DBG_1("Scale: ", m_scale, " tile_scaled:", m_tile_size_scalled.x, ":",
		  m_tile_size_scalled.y);

  m_tile_size_scalled.x = m_ets_file.getTileWidth() * m_scale;
  m_tile_size_scalled.y = m_ets_file.getTileHeight() * m_scale;
  showImage(true);
}

Chunks::Chunks(QWidget *parent) : QLabel(parent) {
  setScaledContents(true);
  setStyleSheet("background-color: red;");
}

Chunks::~Chunks() {}

void Chunks::setViewSize(const uint32_t width, const uint32_t height) {
  m_view_size.x = width;
  m_view_size.y = height;
  DBG_2("view size:", m_view_size.x, ":", m_view_size.y);
  if (!m_ets_file.isOpen()) {
	  setVisible(false);
  }
  else {
  	showImage();
  }
}

void Chunks::fixPosition() {
  if (x() <= -m_tile_size_scalled.x) {
   int32_t skipped_x = -x() / m_tile_size_scalled.x;
	if (skipped_x != 0) {
		m_skipped_tiles.x += skipped_x;
		move(x() + skipped_x * m_tile_size_scalled.x, y());
	}
  }
  else if (x() > 0 && m_skipped_tiles.x > 0 ) {
    int32_t skipped_x = 1 + (x() / m_tile_size_scalled.x);
    if (skipped_x > m_skipped_tiles.x) {
      skipped_x = m_skipped_tiles.x;
      m_skipped_tiles.x = 0;
    } else {
      m_skipped_tiles.x -= skipped_x;
    }
    move(x() - skipped_x * m_tile_size_scalled.x, y());
  }

  if (y() <= -m_tile_size_scalled.y) {
   int32_t skipped_y = -y() / m_tile_size_scalled.y;
	if (skipped_y != 0) {
		m_skipped_tiles.y += skipped_y;
		move(x(), y() + skipped_y * m_tile_size_scalled.y);
	}
  }
  else if (y() > 0 && m_skipped_tiles.y > 0 ) {
    int32_t skipped_y = 1 + (y() / m_tile_size_scalled.y);
    if (skipped_y > m_skipped_tiles.y) {
      skipped_y = m_skipped_tiles.y;
      m_skipped_tiles.y = 0;
    } else {
      m_skipped_tiles.y -= skipped_y;
    }
    move(x(), y() - skipped_y * m_tile_size_scalled.y);
  }
}

void Chunks::showImage(const bool force_reload) {
  if (m_ets_file.isOpen()) {
    DBG_1(__PRETTY_FUNCTION__);
    fixPosition();

    point_t left_top;
    point_t right_bottom;
    float inversed_scale = 1.0f / m_scale;
	DBG_2("inversed scale:", inversed_scale);
    left_top.x = (m_skipped_tiles.x * m_ets_file.getTileWidth()) -
                 (x() * inversed_scale);
    left_top.y = (m_skipped_tiles.y * m_ets_file.getTileHeight()) -
                 (y() * inversed_scale);

    if (left_top.x < 0) {
      left_top.x = 0;
    }
    if (left_top.y < 0) {
      left_top.y = 0;
    }

    right_bottom.x = left_top.x + (m_view_size.x * inversed_scale);
    right_bottom.y = left_top.y + (m_view_size.y * inversed_scale);

    DBG_3("skipped:", m_skipped_tiles.x, ":", m_skipped_tiles.y);
    DBG_3("position:", x(), ":", y());
    DBG_3("my size: ", size().width(), ":", size().height());
    loadFragment(left_top, right_bottom, force_reload);

	if (!isVisible()) {
	  setVisible(true);
	}
  }
}

void Chunks::clear() {
  setVisible(false);
  m_position = QPoint(0,0);
  m_scale = 1.0f;
  m_loaded_tiles[0].x = 0;
  m_loaded_tiles[0].y = 0;
  m_loaded_tiles[1].x = 0;
  m_loaded_tiles[1].y = 0;
  m_skipped_tiles.x = 0;
  m_skipped_tiles.y = 0;
}

void Chunks::openEts(const QString &fileName) {
  clear();
  if (m_ets_file.open(fileName.toStdString())) {
    level_size_t base_level_size = m_ets_file.getLevelSize(0);
	m_xRatio = base_level_size.vertical / base_level_size.horizontal;

	setVisible(true);
    m_tile_size_scalled.x = m_ets_file.getTileWidth() * m_scale;
    m_tile_size_scalled.y = m_ets_file.getTileHeight() * m_scale;
    showImage();
  }
}

void Chunks::loadTiles(const point_t &minimum, const point_t &maximum,
                       const uint32_t level) {
  DBG_1(__PRETTY_FUNCTION__, "minimum:", minimum.x, ":", minimum.y, " maximum:",
        maximum.x, ":", maximum.y);

  QSize tile_size(m_ets_file.getTileWidth(), m_ets_file.getTileHeight());

  uint32_t tiles_vertical = maximum.x - minimum.x + 1;
  uint32_t tiles_horizontal = maximum.y - minimum.y + 1;

  QPixmap *pixmap = new QPixmap(tile_size.width() * tiles_vertical,
                                tile_size.height() * tiles_horizontal);
  pixmap->fill(Qt::transparent);

  QPixmap tile(tile_size.width(), tile_size.height());
  QPainter *painter = new QPainter(pixmap);

  resize(tiles_vertical * tile_size.width(),
         tiles_horizontal * tile_size.height());

  DBG_1("orginal size:", size().width(), ":", size().height());

  for (uint32_t x = 0; x < tiles_vertical; ++x) {
    for (uint32_t y = 0; y < tiles_horizontal; ++y) {
      image_t image =
          m_ets_file.readImage(level, x + minimum.x, y + minimum.y, 0);

      QByteArray bytes(image.data_ptr, image.size);
      if (tile.loadFromData(bytes, "JPG")) {
        painter->drawPixmap(x * tile_size.width(), y * tile_size.height(),
                            tile);
        DBG_2("loaded tile: ", x + minimum.x, ":", y + minimum.y);
      }
    }
  }

  resize(tiles_vertical * m_tile_size_scalled.x,
         tiles_horizontal * m_tile_size_scalled.y);

  DBG_1("scalled size:", size().width(), ":", size().height());
  painter->end();
  setPixmap(*pixmap);

  m_loaded_tiles[0] = minimum;
  m_loaded_tiles[1] = maximum;

  delete pixmap;
  delete painter;
}

bool Chunks::findOptimalLevel() {
  DBG_3(__PRETTY_FUNCTION__);

  level_size_t level_size = m_ets_file.getLevelSize(m_level);
  uint32_t px = level_size.vertical * m_tile_size_scalled.x;
  uint32_t py = level_size.horizontal * m_tile_size_scalled.y;
  uint32_t optimal_level = m_ets_file.findOptimalLevel(px, py);
  bool level_changed = false;

  if (optimal_level != m_level) {
    DBG_1("level changed ", m_level, " -> ", optimal_level);
    m_level = optimal_level;
    level_changed = true;
  }

  return level_changed;
}

void Chunks::loadFragment(const point_t &minimum, const point_t &maximum,
                          bool force_reload) {
  DBG_3(__PRETTY_FUNCTION__);
  point_t tile_min;
  point_t tile_max;

  tile_min.x = minimum.x / m_ets_file.getTileWidth();
  tile_min.y = minimum.y / m_ets_file.getTileHeight();

  tile_max.x = maximum.x / m_ets_file.getTileWidth();
  tile_max.y = maximum.y / m_ets_file.getTileHeight();

  if (findOptimalLevel() && !force_reload) {
	  force_reload = true;
  }

  level_size_t level_size = m_ets_file.getLevelSize(m_level);
  DBG_2("level_size: ", toString(level_size));

  if ((uint32_t)tile_max.x >= level_size.vertical) {
    tile_max.x = level_size.vertical - 1;
  }
  if ((uint32_t)tile_max.y >= level_size.horizontal) {
    tile_max.y = level_size.horizontal - 1;
  }
  if (force_reload || tile_min.x != m_loaded_tiles[0].x ||
      tile_min.y != m_loaded_tiles[0].y || tile_max.x != m_loaded_tiles[1].x ||
      tile_max.y != m_loaded_tiles[1].y) {
    loadTiles(tile_min, tile_max, m_level);
	DBG_3("reloaded");
  }
  else {
  	DBG_3("reload not needed");
  }
}
