#include "mainwindow.h"
#include "debug.h"
#include <QtGui/QPainter>
#include <QtPrintSupport/QPrintDialog>
#include <QtWidgets/QGraphicsScene>
#include <loader/ets/ets.hpp>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), m_draw_area(new ImageArea(this)),
      m_button_open_ptr(new QPushButton("Open ETS", this)) {
  DBG_2(__PRETTY_FUNCTION__);

  setMinimumSize(m_minimal_width, m_minimal_width);
  resize(m_base_width, m_base_height);
  m_draw_area->Resize();
  setStyleSheet("background-color: lightblue;");

  m_button_open_ptr->setGeometry(QRect(QPoint(10, 10), QSize(140, 20)));
  connect(m_button_open_ptr, SIGNAL(released()), this, SLOT(ClickedOpen()));
}

MainWindow::~MainWindow() { DBG_1("Closing"); }

void MainWindow::ClickedOpen() {
  DBG_2("ClickedOpen");
  QString file_name = QFileDialog::getOpenFileName(
      this, "Select file...", QDir::homePath(), "Images (*.ets)");
  m_draw_area->OpenEts(file_name);
}

void MainWindow::resizeEvent(QResizeEvent *event) {
  m_draw_area->Resize();
}
