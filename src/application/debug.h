#ifndef debug_h
#define debug_h

#include <shared/debugs.h>

#define DBG_PREFIX "<MainWindow>"
#define DBG_LEVEL MAIN_WINDOW_DBG_LEVEL

#endif // debug_h
